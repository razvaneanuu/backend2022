# Welcome to AC Back-end!

În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![enter image description here](https://media.giphy.com/media/mGK1g88HZRa2FlKGbz/giphy.gif)

Acum, pentru ceea ce urmează, vă invităm într-o nouă aventură marca SiSC. Voia bună, distracția și momentele frumoase sunt garantate în această călătorie, deci tot ce mai ramâne de făcut este să verificăm că totul este pregătit pentru drumul pe care urmează să îl parcurgem.
**Așa că hai să începem aventura!**

![](https://media.giphy.com/media/8m7FhQS9nPHXChu21U/giphy.gif)

Pentru început, după cum probabil ați realizat, **Git** este o unealtă de bază pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg.
Pentru că ne oferă posibilitatea să menținem și să lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.

![enter image description here](https://media.giphy.com/media/3o6MbhbYBsqTrbP2qQ/giphy.gif)

Puțin mai jos vom lista și **necesarele + materialele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:

_Tutoriale:_

- [**JavaScript basics**](https://www.codecademy.com/learn/introduction-to-javascript)
- **[SQL basics](https://www.edureka.co/blog/sql-basics/)**
- **[SQL basics video](https://www.youtube.com/watch?v=bEtnYWuo2Bw)**
- **[NodeJS basics](https://www.youtube.com/watch?v=U8XF6AFGqlc)**

_Programe de instalat:_

- **[Xampp](https://www.apachefriends.org/ro/download.html)**
  `Precizare: nu îl instalați în C:\Program files`
- **[Visual Studio Code](https://code.visualstudio.com/)**
- **[NodeJS](https://nodejs.org/en/)**
- **[Git](https://git-scm.com/downloads)**

![enter image description here](https://media.giphy.com/media/n69AaUSkbu8RVVuz24/giphy.gif)

# Acum, să trecem la treabă!

## **Ce veți avea de făcut mai exact?**

Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:

- **Linia de comandă** - dacă avem Ubuntu
- **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows
  Vom crea un folder, iar aici vom face practic **o clonă** la tot ceea ce v-am pregătit, folosind comanda:

`git clone REPO_HTTPS_ADDRESS`

În cazul nostru:

`git clone https://gitlab.com/itsisc/ac-back-end-2022.git`

Acum, dacă navigăm în folderul de mai sus...

![enter image description here](https://media.giphy.com/media/bZBmitwUwKtDa/giphy.gif)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.
Mai mult sau mai puțin, acolo regăsim **un formular**, în care există anumite **cerințe**, de care va trebui să ne ocupăm astăzi:

# Prima oprire:

![enter image description here](https://media.giphy.com/media/5WJeTaliN63TTXqVmA/giphy.gif)

Pentru ca distracția să fie maximă avem nevoie de câți mai mulți participanți. Pentru asta ne vom ajuta de formularul menționat mai sus. Din păcate acesta s-a cam mototolit prin bagaje așa că treaba voastră este să îl aduceți la forma sa inițială prin câteva retușuri.

## Ce veți avea de făcut?

![enter image description here](https://media.giphy.com/media/o0vwzuFwCGAFO/giphy.gif)

- Accesați folderul tocmai clonat, apoi accesați **formular1**
- În fereastra de bash rulați comanda:
  `code.`
  O să se deschidă Visual Studio Code automat unde veți găsi formularul cu probleme.

- Rulați modulele **Apache** și **MySQL** din XAMPP Control Panel
- Se crează baza de date **siscit_back_end** cu **Utf8_general_ci la Collation**, nu vă speriați, este extrem de simplu și vă vom arăta cum să faceți asta ;)
- Deschidem terminalul în Visual Studio Code și rulăm următoarele comenzi:

  `cd ./back-end`

  `npm install`

  `npm install -g nodemon`

- Pentru a porni formularul folosiți comanda: `nodemon` sau `npm start`
- Din Visual Studio Code, accesați fișierul **server.js** și analizați problemele apărute astfel încât lumea să se poată alătura aventurii.
- Atunci când în terminal nu se va mai afla nicio eroare, accesați în browserul vostru **localhost:8081**
- În momentul în care datele pasagerilor vor fi introduse fără erori în tabela **pasageri_inscrisi** din baza de date **siscit_back_end** aventura va putea începe.

  > Pentru testare încercați să aveți deschise în paralel formularul din browser și terminalul din Studio Code, iar de fiecare dată când faceți modificări să îl reîncărcați. De asemenea, după modificări, nu uitați să salvați! **Ctrl + S**
  >
  > În plus, pentru a nu pierde timpul, puteți prioritiza funcționalitatea, nu validitatea datelor (ex. adresa **ion@aaa.com** e la fel de bună precum **ionut.popescu@gmail.com** sau **0711222333** e la fel de ok ca **numărul vostru real de telefon**).
  <details>
    <summary>Înca ceva</summary>
    <br>
    <p> Nu uitați că puteți căuta și pe </p>
    <img src="./formular1/front-end/assets/totally-useless/Google.jpg">    
    </details>
  

**Ai îndeplinit cu succes toți pașii?**

![enter image description here](https://media.giphy.com/media/25722M3fXeVKJKw5jf/giphy.gif)

**Felicitări!** Atunci hai să îi anunțăm și pe ceilalți că problema a fost rezolvată și să urcăm codul sursă undeva... Evident, pe Git.

- Mergem pe profilul nostru de **Gitlab** și dăm click pe butonul mare și verde cu **New Project**.

- Numele proiectului va fi **ac back 2022**
- Nivelul de vizibilitate va fi **Public**
- Și vom bifa căsuța pentru **README.md**, pentru a-l clona mai ușor pe local
- \*Opțional, puteți adăuga o descriere scurtă și amuzantă

> Important de menționat că ori de câte ori veți face modificări și veți vrea să le urcați pe Git va trebui să urmați aceiași pași. Deci atenție, pentru că nu e nimic dificil și merită din plin.

- Pentru a verifica dacă există diferențe între repository și folderul local rulăm:

`git status `

- Pentru a adăuga un fișier modificat din listă avem:

`git add FILE_NAME`

- În caz că vrem să le adăugăm pe **toate**:

`git add .`

- Acum putem rula din nou **git status**, să vedem ce fișiere au fost adăugate cu succes.

- Iar pentru ca toate modificările să aibă sens și toți ceilalți colaboratori să înțeleagă care a fost scopul update-ului, trebuie să oferim și un mesaj specific:

`git commit -m "YOUR_MESSAGE_HERE"`

`ex. --> git commit -m "Fixed ERRORS"`

- În final vom folosi o comanda ce va trimite toată informația către proiectul pe care tocmai ce l-ați creat. Sună cam așa:

`git push`

![enter image description here](https://media.giphy.com/media/6MwA2VkzK5uKF2izwd/giphy.gif)

**Link-ul repository-ului creat de voi îl puteți trimite la următoarele adrese de e-mail:**

`matiber24@gmail.com - Matei Mărgineanu, Director departament IT.`

`vladdudu13@gmail.com - Vlad Dumitrescu, Lead divizie Back-End.`

Felicitări, ai trecut cu bine de primul obstacol!

**Următorul popas vine în curând...**
